return {
  'ThePrimeagen/harpoon',
  config = function()
    local mark = require("harpoon.mark")
    local ui = require("harpoon.ui")

    vim.keymap.set('n', 'gh', mark.toggle_file, { desc = 'Mark a file for the harpoon' })
    --vim.keymap.set('n', 'gH', ui.toggle_quick_menu, { desc = 'Toggle harpoon menu' })

    vim.keymap.set('n', '<leader>1', function() ui.nav_file(1) end, { desc = 'Jump to harpooned doc' })
    vim.keymap.set('n', '<leader>2', function() ui.nav_file(2) end, { desc = 'Jump to harpooned doc' })
    vim.keymap.set('n', '<leader>3', function() ui.nav_file(3) end, { desc = 'Jump to harpooned doc' })
    vim.keymap.set('n', '<leader>4', function() ui.nav_file(4) end, { desc = 'Jump to harpooned doc' })
    vim.keymap.set('n', '<leader>5', function() ui.nav_file(5) end, { desc = 'Jump to harpooned doc' })
    vim.keymap.set('n', '<leader>6', function() ui.nav_file(6) end, { desc = 'Jump to harpooned doc' })
    vim.keymap.set('n', '<leader>7', function() ui.nav_file(7) end, { desc = 'Jump to harpooned doc' })
    vim.keymap.set('n', '<leader>8', function() ui.nav_file(8) end, { desc = 'Jump to harpooned doc' })
    vim.keymap.set('n', '<leader>9', function() ui.nav_file(9) end, { desc = 'Jump to harpooned doc' })
  
    require("harpoon").setup({  tabline = true })

  end
}
