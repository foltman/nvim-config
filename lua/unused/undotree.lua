return {
  "jiaoshijie/undotree",
  lazy = false,
  config = function()
  	vim.keymap.set('n', '<leader>U',  require('undotree').toggle, { desc = 'Toggle undo tree' })

    require("undotree").setup { 
 	 	layout = "left_bottom", -- "left_bottom", "left_left_bottom"
  		ignore_filetype = { 'undotree', 'undotreeDiff', 'qf', 'TelescopePrompt', 'spectre_panel', 'tsplayground' },
  		window = { winblend = 30, },
		keymaps = {
	    ['j'] = "move_next",
	    ['k'] = "move_prev",
	    ['J'] = "move_change_next",
	    ['K'] = "move_change_prev",
	    ['<cr>'] = "action_enter",
	    ['p'] = "enter_diffbuf",
	    ['q'] = "quit",
		},
 	}
  end,
}