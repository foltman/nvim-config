return {
	'ionide/Ionide-vim',
	config = function()
    -- Recommended: this makes the hover windows unfocusable.
    -- vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
    -- vim.lsp.handlers.hover, { focusable = false } )

    require('ionide').setup {
      autostart = true,
      capabilities = require('lspconfighelper').capabilities,
      on_attach = require('lspconfighelper').on_attach,
      flags = { debounce_text_changes = 150, },
    }
  end
}
