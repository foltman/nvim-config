return {
	  'ggandor/leap.nvim',
	  config = function ()
	  	require('leap').add_default_mappings()
	  	-- s - forward, S - backward
	  	-- :h leap-default-mappings
	  end
}
