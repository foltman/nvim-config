return {
    'navarasu/onedark.nvim',
    priority = 1000,
    config = function()
        require('onedark').setup {
            style = 'warm',
            toggle_style_list = { 'light', 'warm' }
        }

        vim.cmd.colorscheme 'onedark'
        vim.keymap.set('n', '<leader>t', function() require("onedark").toggle() end, { desc = 'Toggle [t]heme colorscheme' })

        -- for lualine
        require('lualine').setup({
            options = {
                theme = 'onedark',
            },
        })
    end,
}
