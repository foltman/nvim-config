-- Adds git related signs to the gutter, as well as utilities for managing changes
return {
  'lewis6991/gitsigns.nvim',
  opts = {
    -- See `:help gitsigns.txt`
    signs = {
      add = { text = '+' },
      change = { text = '~' },
      delete = { text = '_' },
      topdelete = { text = '‾' },
      changedelete = { text = '~' },
    },
    on_attach = function(bufnr)
      local gs = require('gitsigns')

      local function map(mode, l, r, opts)
        opts = opts or {}
        opts.buffer = bufnr
        vim.keymap.set(mode, l, r, opts)
      end

      -- Navigation
      vim.keymap.set('n', ']c', function()
        if vim.wo.diff then return ']c' end
        vim.schedule(function() gs.next_hunk() end)
        return '<Ignore>'
      end, { expr = true, desc = 'Git: Next hunk' })

      vim.keymap.set('n', '[c', function()
        if vim.wo.diff then return '[c' end
        vim.schedule(function() gs.prev_hunk() end)
        return '<Ignore>'
      end, { expr = true , desc = 'Git: Previous hunk'})

      -- Actions
      vim.keymap.set('n', '<leader>hs', gs.stage_hunk, { desc = 'Git: stage hunk' } )
      vim.keymap.set('n', '<leader>hr', gs.reset_hunk, { desc = 'Git: reset hunk' } )
      vim.keymap.set('v', '<leader>hs', function() gs.stage_hunk { vim.fn.line('.'), vim.fn.line('v') } end, { desc = 'Git: stage hunk' } )
      vim.keymap.set('v', '<leader>hr', function() gs.reset_hunk { vim.fn.line('.'), vim.fn.line('v') } end, { desc = 'Git: reset hunk' } )
      vim.keymap.set('n', '<leader>hS', gs.stage_buffer, { desc = 'Git: stage buffer' } )
      vim.keymap.set('n', '<leader>hu', gs.undo_stage_hunk, { desc = 'Git: undo stage hunk' } )
      vim.keymap.set('n', '<leader>hR', gs.reset_buffer, { desc = 'Git: reset buffer' } )
      vim.keymap.set('n', '<leader>hp', gs.preview_hunk, { desc = 'Git: preview hunk' } )
      vim.keymap.set('n', '<leader>hb', function() gs.blame_line { full = true } end, { desc = 'Git: blame line' } )
      vim.keymap.set('n', '<leader>tb', gs.toggle_current_line_blame, { desc = 'Git: toggle blame line' } )
      vim.keymap.set('n', '<leader>hd', gs.diffthis, { desc = 'Git: diff this' } )
      vim.keymap.set('n', '<leader>hD', function() gs.diffthis('~') end, { desc = 'Git: diff this' } )
      vim.keymap.set('n', '<leader>td', gs.toggle_deleted, { desc = 'Git: toggle deleted' } )

      -- Text object
      vim.keymap.set({ 'o', 'x' }, 'ih', ':<C-U>Gitsigns select_hunk<CR>', { desc = 'Git: Select hunk'})
    end
    --on_attach = function(bufnr)
    --  vim.keymap.set('n', '<leader>gp', require('gitsigns').prev_hunk, { buffer = bufnr, desc = '[G]o to [P]revious Hunk' })
    --  vim.keymap.set('n', '<leader>gn', require('gitsigns').next_hunk, { buffer = bufnr, desc = '[G]o to [N]ext Hunk' })
    --  vim.keymap.set('n', '<leader>ph', require('gitsigns').preview_hunk, { buffer = bufnr, desc = '[P]review [H]unk' })
    --end,
  },
}

