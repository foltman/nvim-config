return {
	'echasnovski/mini.files',
	version = false,
	config = function()
		require('mini.files').setup()
      	vim.keymap.set('n', '<leader>f', MiniFiles.open, { buffer = bufnr, desc = '[F]iles' })

	end
}